$(function(){
    wizard = $("#form-total").steps({
        headerTag: "h2",
        bodyTag: "section",
        transitionEffect: "fade",
        enableAllSteps: true,
        autoFocus: true,
        transitionEffectSpeed: 500,
        titleTemplate : '<span class="title">#title#</span>',
        labels: {
            previous : 'Atras',
            next : 'Siguiente',
            finish : 'Finalizar',
            current : ''
        },
        onStepChanging: function (event, currentIndex, newIndex) {
            if(newIndex > currentIndex){
                if(currentIndex == 0){
                    if($("#tipoNegocio").val() == ""){
                        swal({
                            imageUrl: "img/cara-incorrecto.png",
                            imageWidth: 150,
                            imageHeight: 150,
                            title: "Por favor seleccione un tipo de negocio",
                            showConfirmButton: true,
                            confirmButtonText: "Cerrar"
                        }).then(function(result){
                            return false;
                        });
                        return false;
                    }
                }
                if(currentIndex == 1){
                    var mensaje = validarPasoDos();
                    if(mensaje != ""){
                        swal({
                            imageUrl: "img/cara-incorrecto.png",
                            imageWidth: 150,
                            imageHeight: 150,
                            title: mensaje,
                            showConfirmButton: true,
                            confirmButtonText: "Continuar"
                        }).then(function(result){
                            return false;
                        });
                        return false;
                    }
                }
                if(currentIndex == 2){
                    if($("#nombreTienda").val() == "" || $("#paisEmpresa").val() < 1 || $("#monedaEmpresa").val().length < 1){
                        swal({
                            imageUrl: "img/cara-incorrecto.png",
                            imageWidth: 150,
                            imageHeight: 150,
                            title: "Por favor complete todos los campos obligatorios",
                            showConfirmButton: true,
                            confirmButtonText: "Cerrar"
                        }).then(function(result){
                            return false;
                        });
                        return false;
                    }
                }
            }

            return true;
        },
        onFinished: function (event, currentIndex) {
            finalizar();
        }
    });

    wizard.steps('previous');

    if ($('.wizard-carousel').length) {
        $('.wizard-carousel').owlCarousel({
            loop: false,
            margin: 30,
            items: 4,
            nav: true,
            navText: ['<img src="img/flecha-izquierda.png" width="50px" height="70px">','<img src="img/flecha-derecha.png" width="50px" height="70px">'],
            dots: false,
            responsiveClass: true,
            slideSpeed: 300,
            responsive: {
                0: {
                    items: 1
                },
                700: {
                    items: 4
                }
            }
        })
    }

    Array.prototype.forEach.call(document.body.querySelectorAll("*[data-mask]"), applyDataMask);

    function applyDataMask(field) {
        var mask = field.dataset.mask.split('');
        
        function stripMask(maskedData) {
            function isDigit(char) {
                return /\d/.test(char);
            }
            return maskedData.split('').filter(isDigit);
        }
        
        function applyMask(data) {
            return mask.map(function(char) {
                if (char != '_') return char;
                if (data.length == 0) return char;
                return data.shift();
            }).join('')
        }
        
        function reapplyMask(data) {
            return applyMask(stripMask(data));
        }
        
        function changed() {   
            var oldStart = field.selectionStart;
            var oldEnd = field.selectionEnd;
            
            field.value = reapplyMask(field.value);
            
            field.selectionStart = oldStart;
            field.selectionEnd = oldEnd;
        }
        
        field.addEventListener('click', changed)
        field.addEventListener('keyup', changed)
    }
});

function finalizar() {
    var tipoNegocio = $("#tipoNegocio").val();
    var rucEmpresa = $("#rucEmpresa").val();
    var razonSocialEmpresa = $("#razonSocialEmpresa").val();
    var telefonoEmpresa = $("#telefonoEmpresa").val();
    var emailEmpresa = $("#emailEmpresa").val();
    var propinaEmpresa = $("#propinaEmpresa:checked").length;
    var nombreTienda = $("#nombreTienda").val();
    var plan = $("#plan").val();
    var paisEmpresa = $("#paisEmpresa").val();
    var monedaEmpresa = $("#monedaEmpresa").val();

    datos = {
        "tipoNegocio" : tipoNegocio,
        "rucEmpresa" : rucEmpresa,
        "razonSocialEmpresa" : razonSocialEmpresa,
        "telefonoEmpresa" : telefonoEmpresa,
        "emailEmpresa" : emailEmpresa,
        "propinaEmpresa" : propinaEmpresa,
        "nombreTienda" : nombreTienda,
        "plan" : plan,
        "paisEmpresa" : paisEmpresa,
        "monedaEmpresa" : monedaEmpresa
    }

    if(parseInt(plan) == 1){
        $.ajax({

            url: 'servicio_uno.php',
            type: 'POST',
            data: datos,
            success: function(result){
                swal({
                    imageUrl: "img/cara-correcto.png",
                    imageWidth: 150,
                    imageHeight: 150,
                      title: "Los datos fueron guardados exitosamente",
                      showConfirmButton: true,
                      confirmButtonText: "Cerrar"
                }).then(function(result){
                    if (result.value) {
                        window.location = "index.html";
                    }
                });
            }
    
        });
    }else if(parseInt(plan) == 2){
        $.ajax({

            url: 'servicio_dos.php',
            type: 'POST',
            data: datos,
            success: function(result){
                swal({
                    imageUrl: "img/cara-correcto.png",
                    imageWidth: 150,
                    imageHeight: 150,
                      title: "Los datos fueron guardados exitosamente",
                      showConfirmButton: true,
                      confirmButtonText: "Cerrar"
                }).then(function(result){
                    if (result.value) {
                        window.location = "index.html";
                    }
                });
            }
    
        });
    }else if(parseInt(plan) == 3){
        $.ajax({

            url: 'servicio_tres.php',
            type: 'POST',
            data: datos,
            success: function(result){
                swal({
                    imageUrl: "img/cara-correcto.png",
                    imageWidth: 150,
                    imageHeight: 150,
                      title: "Los datos fueron guardados exitosamente",
                      showConfirmButton: true,
                      confirmButtonText: "Cerrar"
                }).then(function(result){
                    if (result.value) {
                        window.location = "index.html";
                    }
                });
            }
    
        });
    }
}

function validarPasoDos(){
    
    var uno = $("#rucEmpresa").val();
    var dos = $("#razonSocialEmpresa").val();
    var tres = $("#telefonoEmpresa").val();
    var cuatro = $("#emailEmpresa").val();
    var mensaje = "Por favor complete todos los campos obligatorios";

    if(uno != "" && dos != "" && tres != "" && cuatro != ""){
        mensaje = "";
    }
    
    if(uno != ""){
        mensaje = validarRucEmpresa(uno);
    }

    if(cuatro != ""){
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{1,8})?$/;
        var valido = emailReg.test(cuatro);
        if(!valido){
            mensaje = "El email ingresado no es valido";
        }

        $.ajax({
            url: 'validaciones.php',
            type: 'POST',
            async: false,
            data: { "validarEmail": cuatro },
            success: function(result){
                ans = JSON.parse(result);
                if(ans.length > 0){
                    mensaje = "El email ya se encuentra registrado";
                }
            }
        });
    }

    return mensaje;
}

function validarRucEmpresa(ruc){
    var texto = "";
    if(ruc.length != 11){
        texto = "El RUC debe tener 11 caracteres";
    }else{
        $.ajax({
            url: 'validaciones.php',
            type: 'POST',
            async: false,
            data: { "validarRuc": ruc },
            success: function(result){
                ans = JSON.parse(result);
                if(ans.length > 0){
                    texto = "El RUC ya se encuentra registrado";
                }
            }
        });
    }
    return texto;
}


$(document).ready(function(){
    
    $(".wizard-carousel-item").on("click",function(){
        var tarjetas = $(".wizard-carousel-item");
        var miTarjeta = $(this);

        tarjetas.each(function(index, element){
            if(tarjetas[index] == miTarjeta[0]){
                $(this).removeClass('card-1').addClass('card-00');
                var hijos = $(this).children();
                $('#tipoNegocio').val(hijos[1].innerHTML);
		
            }else{
                $(this).removeClass('card-00').addClass('card-1');
            }
        });
    });

    datos = {
        "paises": "si"
    }

    $.ajax({

        url: 'cargarPaises.php',
        type: 'POST',
        data: datos,
        success: function(result){
            paises = JSON.parse(result);
            paises.forEach(pais => {
                $("#paisEmpresa").append("<option value='"+pais.id+"'>"+pais.pais+"</option>");
            });
            $('#paisEmpresa option:contains("PERU")').prop('selected',true).trigger('change');
        }

    });

    $("#rucEmpresa").on("keyup",function(){
        if($(this).val().length == 11){
            $.ajax({
                url: 'validaciones.php',
                type: 'POST',
                data: { "validarRuc": $(this).val() },
                success: function(result){
                    ans = JSON.parse(result);
                    if(ans.length > 0){
                        swal({
                            imageUrl: "img/cara-incorrecto.png",
                            imageWidth: 150,
                            imageHeight: 150,
                              title: "El RUC ya se encuentra registrado",
                              showConfirmButton: true,
                              confirmButtonText: "Cerrar"
                        }).then(function(result){
                            
                        });
                    }
                }
            });
        }
    });

    $("#emailEmpresa").on("keyup", function(){
        $.ajax({
            url: 'validaciones.php',
            type: 'POST',
            data: { "validarEmail": $(this).val() },
            success: function(result){
                ans = JSON.parse(result);
                if(ans.length > 0){
                    swal({
                        imageUrl: "img/cara-incorrecto.png",
                        imageWidth: 150,
                        imageHeight: 150,
                          title: "El email ya se encuentra registrado",
                          showConfirmButton: true,
                          confirmButtonText: "Cerrar"
                    }).then(function(result){
                        
                    });
                }
            }
        });
    });

    $("#paisEmpresa").on("change",function(){
        datos = {
            "monedas": "si",
            "idPais" : $(this).val()
        }
        
        $.ajax({
    
            url: 'cargarPaises.php',
            type: 'POST',
            data: datos,
            success: function(result){
                $("#monedaEmpresa").empty();
                monedas = JSON.parse(result);
                monedas.forEach(moneda => {
                    $("#monedaEmpresa").append("<option value='"+moneda.simbolo+"'>"+moneda.simbolo+"</option>");
                });
            }
    
        }); 
    });

    
});
