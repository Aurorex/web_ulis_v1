$(function() {
  "use strict";

  var nav_offset_top = $('header').height() + 50; 
    /*-------------------------------------------------------------------------------
	  Navbar 
	-------------------------------------------------------------------------------*/

	//* Navbar Fixed  
    function navbarFixed(){
        if ( $('.header_area').length ){ 
            $(window).scroll(function() {
                var scroll = $(window).scrollTop();   
                if (scroll >= nav_offset_top ) {
                    $(".header_area").addClass("navbar_fixed");
                } else {
                    $(".header_area").removeClass("navbar_fixed");
                }
            });
        };
    };
    navbarFixed();

    $("#linkSoluciones").click(function (){
        $('html, body').animate({
            scrollTop: $("#soluciones").offset().top
        }, 1000);
    });

    $("#linkFuncionalidades").click(function (){
        $('html, body').animate({
            scrollTop: $("#funcionalidades").offset().top - 110
        }, 1000);
    });

    $("#linkPlanes").click(function (){
        $('html, body').animate({
            scrollTop: $("#planes").offset().top - 50
        }, 1000);
    });

    $(window).scroll(function(){
        if ( $(this).scrollTop() > 70 ) {
            $("#btnTop").fadeIn();
        }else{
            $("#btnTop").fadeOut();
        }
    });
    
    $("#btnTop").click(function (){
        $('html, body').animate({
            scrollTop: 0
        }, 2000);
    });


  //------- mailchimp --------//  
	function mailChimp() {
		$('#mc_embed_signup').find('form').ajaxChimp();
	}
  mailChimp();


  /*-------------------------------------------------------------------------------
	  testimonial slider
	-------------------------------------------------------------------------------*/
    if ($('.testimonial').length) {
        $('.testimonial').owlCarousel({
            loop: true,
            margin: 30,
            // items: 5,
            nav: false,
            dots: true,
            responsiveClass: true,
            slideSpeed: 300,
            paginationSpeed: 500,
            responsive: {
                0: {
                    items: 1
                },
                800: {
                    items: 2
                },
                1200: {
                    items: 3
                }
            }
        })
    }



    /*-------------------------------------------------------------------------------
	  featured slider
	-------------------------------------------------------------------------------*/
    if ($('.logo-carousel').length) {
        $('.logo-carousel').owlCarousel({
            loop: true,
            margin: 30,
            items: 2,
            nav: false,
            dots: true,
            responsiveClass: true,
            slideSpeed: 300,
            autoplay:true,
            autoplayTimeout:3000,
            autoplayHoverPause:true,
            responsive: {
                0: {
                    items: 1
                },
                700: {
                    items: 2
                }
            }
        })
    }



    /*-------------------------------------------------------------------------------
	  featured slider
	-------------------------------------------------------------------------------*/
    if ($('.hero-carousel').length) {
        $('.hero-carousel').owlCarousel({
            loop: false,
            margin: 30,
            items: 1,
            nav: false,
            dots: true,
            responsiveClass: true,
            slideSpeed: 300,
            paginationSpeed: 500
        })
    }



  
});


