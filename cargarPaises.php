<?php

require_once "conexion.php";

if(isset($_POST["paises"])){
    
    $query = "SELECT id, pais FROM monedas WHERE id NOT IN (2,3)";
    $stmt = Conexion::conectar()->prepare($query);
    $stmt -> execute();
    $paises = $stmt -> fetchAll();

    echo json_encode($paises);

}elseif (isset($_POST["monedas"])) {

    $idPais = $_POST["idPais"];
    $query = "SELECT simbolo FROM monedas WHERE id = $idPais OR id IN (2,3)";
    $stmt = Conexion::conectar()->prepare($query);
    $stmt -> execute();
    $monedas = $stmt -> fetchAll();

    echo json_encode($monedas);
}

?>